# 540-Fall2023-Examples

Example code illustrating the use of code to control Raspberry Pi sensors & actuators.

## Getting started

To run, use `python filename.py`

## Documentation
Raspberry Pi Camera (Hardware): https://www.raspberrypi.com/documentation/accessories/camera.html#getting-started

Pi Camera2 (Python library): https://datasheets.raspberrypi.com/camera/picamera2-manual.pdf 

### Thread examples:  
```
python threads_example.py
python threads_example_interrupt.py
python threads_example_multiple_classes.py
```   
Links:  
- Check in the end (thread-safe class): https://www.pythontutorial.net/python-concurrency/python-threading-lock/  
- **How to kill a Python thread:** https://youtu.be/A97QLHAqNuw?si=q53UWIOyDmntsstf  

### Sockets examples:   <======= UPDATED ========
The following examples are adapted from: https://www.digitalocean.com/community/tutorials/python-socket-programming-server-client
```
python 02-socket_server.py
python 02-socket_client.py
python 02-jwt.py
```